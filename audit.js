
var today = new Date();
var http = require('http');
//Sum of bank accounts


//request options
var options = {
  host: 'localhost', //IP address of the server
  path: '/audit/?token=' + String(today.getDate()).padStart(2, '0') + "ABC" + today.getFullYear() + "XYZ" + String(today.getMonth() + 1).padStart(2, '0'),
  port: '1234',
  method: 'GET'
};

//Get request for all bank accounts
var req = http.request(options, readResponse);
req.end();

function readResponse(response) {
  var responseData = '';
  response.on('data', function (chunk) {
    responseData += chunk;
  });
  response.on('end', function() {
	var dataObj = JSON.parse(responseData);
    console.log("Message: " + dataObj.msg);
  });
}